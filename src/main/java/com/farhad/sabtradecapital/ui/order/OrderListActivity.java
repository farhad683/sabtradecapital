package com.farhad.sabtradecapital.ui.order;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.farhad.sabtradecapital.CustomerListActivity;
import com.farhad.sabtradecapital.R;

import java.util.ArrayList;

public class OrderListActivity extends AppCompatActivity {
    private RecyclerView rvOrderList;
    private ArrayList<OrderModel> orderLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        setTitle("ORDER LIST");

//        initializeOrders();
    }
    /*private void initializeOrders() {
        rvOrderList = findViewById(R.id.order_list_rv_details);
        orderLists = new ArrayList<>();
        orderLists.add(new OrderModel(1, "3rd May", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        orderLists.add(new OrderModel(2, "2nd May", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        orderLists.add(new OrderModel(1, "3rd May", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        orderLists.add(new OrderModel(2, "2nd May", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        orderLists.add(new OrderModel(1, "3rd May", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        orderLists.add(new OrderModel(2, "2nd May", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        orderLists.add(new OrderModel(1, "3rd May", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        orderLists.add(new OrderModel(2, "2nd May", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        OrderListAdapter orderListAdapter = new OrderListAdapter(this, orderLists);

        rvOrderList.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvOrderList.setLayoutManager(layoutManager);
        rvOrderList.setAdapter(orderListAdapter);
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.customer_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_order_list:
                break;
            case R.id.menu_customer_list:
                startActivity(new Intent(this, CustomerListActivity.class));
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
