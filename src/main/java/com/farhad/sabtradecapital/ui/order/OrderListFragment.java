package com.farhad.sabtradecapital.ui.order;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital.R;
import com.farhad.sabtradecapital.adapter.OrderListAdapter;

import java.util.ArrayList;

public class OrderListFragment extends Fragment {
    private RecyclerView rvOrderList;
    private ArrayList<OrderModel> orderLists;

    private OrderViewModel orderViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        orderViewModel =
                ViewModelProviders.of(this).get(OrderViewModel.class);
        View root = inflater.inflate(R.layout.activity_order_list, container, false);

        initializeOrders(root);
        /*final TextView textView = root.findViewById(R.id.text_slideshow);
        orderViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/
        return root;
    }

    private void initializeOrders(View view) {
        rvOrderList = view.findViewById(R.id.activity_order_list_rv_details);
        orderLists = new ArrayList<>();
        orderLists.add(new OrderModel(1, "03-05-2020", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        orderLists.add(new OrderModel(2, "03-05-2020", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        orderLists.add(new OrderModel(1, "03-05-2020", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        orderLists.add(new OrderModel(2, "03-05-2020", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        orderLists.add(new OrderModel(1, "03-05-2020", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        orderLists.add(new OrderModel(2, "03-05-2020", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        orderLists.add(new OrderModel(1, "03-05-2020", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        orderLists.add(new OrderModel(2, "03-05-2020", "Nayeem P.P", "Nayeem", "Seller", 200.0, "address"));
        OrderListAdapter orderListAdapter = new OrderListAdapter(getContext(), orderLists);

        rvOrderList.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvOrderList.setLayoutManager(layoutManager);
        rvOrderList.setAdapter(orderListAdapter);
    }
}
