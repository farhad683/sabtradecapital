package com.farhad.sabtradecapital.ui.order;

public class OrderModel {
    private int id;
    private String date;
    private String shopName;
    private String customerName;
    private String sellBy;
    private double totalAmount;
    private String address;

    public OrderModel(int id, String date, String shopName, String customerName, String sellBy, double totalAmount, String address) {
        this.id = id;
        this.date = date;
        this.shopName = shopName;
        this.customerName = customerName;
        this.sellBy = sellBy;
        this.totalAmount = totalAmount;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getSellBy() {
        return sellBy;
    }

    public void setSellBy(String sellBy) {
        this.sellBy = sellBy;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
