package com.farhad.sabtradecapital.ui.report;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital.R;
import com.farhad.sabtradecapital.adapter.ReportAdapter;
import com.farhad.sabtradecapital.models.ReportModel;

import java.util.ArrayList;
import java.util.List;

public class AllReportFragment extends Fragment {

    public AllReportFragment() {
        // Required empty public constructor
    }

    private List<ReportModel> reportModelList;
    private ReportAdapter reportAdapter;
    private RecyclerView reportRecyclerView;

    private Button reportTodayBtn, btnToDate, btnFromDate;
    private TextView reportTitleTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_report, container, false);

        reportTodayBtn = view.findViewById(R.id.report_today_btn);
        reportTitleTextView = view.findViewById(R.id.report_tv_title);
        btnFromDate = view.findViewById(R.id.all_report_from_date_btn);
        btnToDate = view.findViewById(R.id.all_report_to_date_btn);

        btnFromDate.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                DatePicker datePicker = new DatePicker(getContext());
                final DatePickerDialog dialog = new DatePickerDialog(getContext());
                dialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String date = dayOfMonth + "/"+month+"/"+year;
                        btnFromDate.setText(date);
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });

        btnToDate.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                DatePicker datePicker = new DatePicker(getContext());
                final DatePickerDialog dialog = new DatePickerDialog(getContext());
                dialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String date = dayOfMonth + "/"+month+"/"+year;
                        btnToDate.setText(date);
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });

        reportRecyclerView = view.findViewById(R.id.report_all_recycler_view);
        setAllReportRecyclerView();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        reportTodayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTodayReportList();
                reportTodayBtn.setVisibility(View.GONE);
            }
        });
    }

    private void setAllReportRecyclerView() {
        List<ReportModel> list = new ArrayList<>();
        list = new ArrayList<>();
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        reportRecyclerView.setHasFixedSize(true);
        reportRecyclerView.setLayoutManager(layoutManager);
        reportAdapter = new ReportAdapter(list);
        reportRecyclerView.setAdapter(reportAdapter);
        reportAdapter.notifyDataSetChanged();

    }

    private void setTodayReportList() {
        reportTitleTextView.setText("TODAY'S SALES REPORT");
        List<ReportModel> list = new ArrayList<>();
        list = new ArrayList<>();
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));
        list.add(new ReportModel("2-Apr-2020", "Nayeem P.P Farm", 1.0, 1.0, 1.0));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        reportRecyclerView.setHasFixedSize(true);
        reportRecyclerView.setLayoutManager(layoutManager);
        reportAdapter = new ReportAdapter(list);
        reportRecyclerView.setAdapter(reportAdapter);
        reportAdapter.notifyDataSetChanged();
    }
}
