package com.farhad.sabtradecapital.ui;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.farhad.sabtradecapital.R;
import com.farhad.sabtradecapital.adapter.TicketAdapter;
import com.farhad.sabtradecapital.models.TicketItem;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TicketFragment extends Fragment {

    private RecyclerView rv_ticket;
    private List<TicketItem> listTicket;

    public TicketFragment() {
        // Required empty public constructor
    }

    private Button ticketSubmitBtn;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View root = inflater.inflate(R.layout.fragment_ticket, container, false);

        rv_ticket = root.findViewById(R.id.ticket_rv_list);
        ticketSubmitBtn = root.findViewById(R.id.ticket_submit_btn);

        setRecycler(root);

        ticketSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Data Successfully Saved", Toast.LENGTH_SHORT).show();
            }
        });



        return root;
    }

    private void setRecycler(View root) {
        listTicket = new ArrayList<TicketItem>();
        listTicket.add(new TicketItem("Oil 500ml", 200, 4000.0));
        listTicket.add(new TicketItem("Water 500ml", 300, 5000.0));
        listTicket.add(new TicketItem("Miniket 40kg", 200, 6000.0));
        listTicket.add(new TicketItem("Ata", 2, 60.0));

        rv_ticket.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rv_ticket.setLayoutManager(layoutManager);
        rv_ticket.setAdapter(new TicketAdapter(getContext(), listTicket));
    }
}
