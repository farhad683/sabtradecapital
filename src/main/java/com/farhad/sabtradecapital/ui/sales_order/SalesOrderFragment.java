package com.farhad.sabtradecapital.ui.sales_order;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.farhad.sabtradecapital.MainActivity;
import com.farhad.sabtradecapital.R;
import com.farhad.sabtradecapital.ui.TicketFragment;
import com.farhad.sabtradecapital.ui.customer.AddCustomerDialog;
import com.farhad.sabtradecapital.ui.home.HomeFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class SalesOrderFragment extends Fragment implements AddCustomerDialog.DialogListener {
    private Button sales_order_btn_save, goBackBtn;
    private ImageButton imgButtonAddCustomer;
    private String m_Text = "";
    

    public SalesOrderFragment() {
        // Required empty public constructor
    }
    private TextView addQuantity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_sales_order, container, false);

        addQuantity = root.findViewById(R.id.sales_order_add_quantity_btn);
        goBackBtn = root.findViewById(R.id.sales_order_go_back_btn);
        sales_order_btn_save = root.findViewById(R.id.sales_order_btn_save);
        imgButtonAddCustomer = root.findViewById(R.id.image_button_add_customer);
        sales_order_btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment newFragment = new TicketFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.nav_host_fragment, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });
        imgButtonAddCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*AddCustomerDialog dialog = new AddCustomerDialog();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame, dialog);
                ft.commit();*/
                showDialog();
            }
        });
        goBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment newFragment = new HomeFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.nav_host_fragment, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        
        addQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInputDialog();
            }
        });

        return root;
    }

    private void showInputDialog() {
        final Dialog signInDialog = new Dialog(getContext());
        signInDialog.setContentView(R.layout.add_quantity_dialog_layout);
        signInDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        signInDialog.setCancelable(true);
        final Button dialogOK = signInDialog.findViewById(R.id.add_quantity_ok_btn);

        dialogOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInDialog.dismiss();
                Toast.makeText(getContext(), "Quantity Saved", Toast.LENGTH_SHORT).show();
            }
        });
        
        signInDialog.show();
    }

    private void showDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Customer");
// I'm using fragment here so I'm using getView() to provide ViewGroup
// but you can provide here any other instance of ViewGroup from your Fragment / Activity
        final View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.dialog_customer_form, (ViewGroup) getView(), false);
// Set up the input
        final EditText edtCustomerName = viewInflated.findViewById(R.id.edt_add_customer_name);
        final EditText edtShopName = viewInflated.findViewById(R.id.edt_shop_name);
        final Spinner spArea = viewInflated.findViewById(R.id.sp_customer_area);
        final Spinner spZone = viewInflated.findViewById(R.id.sp_customer_zone);
        final EditText edtPhone = viewInflated.findViewById(R.id.edt_customer_phone);
        final EditText edtAddress = viewInflated.findViewById(R.id.edt_customer_address);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        builder.setView(viewInflated);

// Set up the buttons
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String customerName = edtCustomerName.getText().toString();
                String shopName = edtShopName.getText().toString();
                String phone = edtPhone.getText().toString();
                String address = edtAddress.getText().toString();
                if(!checkValidation(customerName, shopName, phone, address)){
                    dialog.dismiss();
                } else {
                    Toast.makeText(getContext(), "Enter the data first", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private boolean checkValidation(String customerName, String shopName, String phone, String address) {
        if(customerName.equals("") || shopName.equals("") || phone.equals("") || address.equals("")){
            return false;
        }
        return true;
    }


    @Override
    public void onFinishEditDialog(String inputText) {
        if (TextUtils.isEmpty(inputText)) {
//            textView.setText("Email was not entered");
            Toast.makeText(getContext(), "Email was not entered", Toast.LENGTH_SHORT).show();
        } else {
//            textView.setText("Email entered: " + inputText);
            Toast.makeText(getContext(), "Email  entered " + inputText, Toast.LENGTH_SHORT).show();
        }
    }
}
