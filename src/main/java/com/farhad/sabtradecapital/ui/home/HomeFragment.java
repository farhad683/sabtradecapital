package com.farhad.sabtradecapital.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.farhad.sabtradecapital.R;
import com.farhad.sabtradecapital.ui.StockFragment;
import com.farhad.sabtradecapital.ui.customer.CustomerDetailsFragment;
import com.farhad.sabtradecapital.ui.customer.CustomerFragment;
import com.farhad.sabtradecapital.ui.order.OrderListFragment;
import com.farhad.sabtradecapital.ui.report.AllReportFragment;
import com.farhad.sabtradecapital.ui.sales_order.SalesOrderFragment;
import com.farhad.sabtradecapital.ui.stock.StockActivity;

public class HomeFragment extends Fragment implements View.OnClickListener {
    private LinearLayout lt_total_order, lt_total_customer, lt_sales_order, lt_total_sales, lt_report, lt_stock;

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_home, container, false);

        lt_total_order = root.findViewById(R.id.lt_total_order);
        lt_total_customer = root.findViewById(R.id.lt_customer);
        lt_sales_order = root.findViewById(R.id.lt_sales_order);
        lt_total_sales = root.findViewById(R.id.lt_total_sales);
        lt_report = root.findViewById(R.id.lt_report);
        lt_stock = root.findViewById(R.id.lt_stock);


        lt_total_order.setOnClickListener(this);
        lt_total_customer.setOnClickListener(this);
        lt_sales_order.setOnClickListener(this);
        lt_total_sales.setOnClickListener(this);
        lt_report.setOnClickListener(this);
        lt_stock.setOnClickListener(this);
        /*final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/
        return root;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        switch (id){
            case R.id.lt_total_order:
                fm.beginTransaction().addToBackStack(null).replace(R.id.nav_host_fragment, new OrderListFragment()).commit();
                break;
            case R.id.lt_customer:
                fm.beginTransaction().addToBackStack(null).replace(R.id.nav_host_fragment, new CustomerFragment()).commit();
                break;
            case R.id.lt_sales_order:
                fm.beginTransaction().addToBackStack(null).replace(R.id.nav_host_fragment, new SalesOrderFragment()).commit();
                break;
            case R.id.lt_total_sales:
                fm.beginTransaction().addToBackStack(null).replace(R.id.nav_host_fragment, new SalesOrderFragment()).commit();
                break;
            case R.id.lt_report:
                fm.beginTransaction().addToBackStack(null).replace(R.id.nav_host_fragment, new AllReportFragment()).commit();
                break;
            case R.id.lt_stock:
//                fm.beginTransaction().addToBackStack(null).replace(R.id.nav_host_fragment, new StockFragment()).commit();

                startActivity(new Intent(getContext(), StockActivity.class));
                break;

        }
    }
}
