package com.farhad.sabtradecapital.ui.customer;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.farhad.sabtradecapital.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerDailyDeitalFragment extends Fragment {

    public CustomerDailyDeitalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_customer_daily_deital, container, false);
    }
}
