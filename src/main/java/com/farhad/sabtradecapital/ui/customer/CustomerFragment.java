package com.farhad.sabtradecapital.ui.customer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital.R;
import com.farhad.sabtradecapital.adapter.CustomerListAdapter;
import com.farhad.sabtradecapital.customer.CustomerItem;

import java.util.ArrayList;

public class CustomerFragment extends Fragment {
    public String[] areaList = {"Dhaka", "Tangail", "Mirpur"};
    public String[] zoneList = {"zone 1", "zone 2", "zone 3"};
    public String[] filterList = {"filter 1", "filter 2", "filter 3"};

    private Spinner spFilter, spArea, spZone;
    private RecyclerView rvCustomerList;

    ArrayList<CustomerItem> mCustomerItemList;

    private CustomerViewModel customerViewModel;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        customerViewModel =
                ViewModelProviders.of(this).get(CustomerViewModel.class);
        View root = inflater.inflate(R.layout.activity_customer_list, container, false);


        initializeCustomers(root);
        initializeFilters(root);



        /*final TextView textView = root.findViewById(R.id.text_home);
        customerViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/
        return root;
    }

    private void initializeFilters(View v) {
        spFilter = v.findViewById(R.id.customer_list_sp_filter);
        spArea = v.findViewById(R.id.customer_list_sp_area);
        spZone = v.findViewById(R.id.customer_layout_sp_zone);

        ArrayAdapter filterAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, filterList);
        ArrayAdapter areaAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, areaList);
        ArrayAdapter zoneAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, zoneList);

        spFilter.setAdapter(filterAdapter);
        spArea.setAdapter(filterAdapter);
        spZone.setAdapter(zoneAdapter);
    }


    private void initializeCustomers(View v) {
        rvCustomerList = v.findViewById(R.id.customer_list_rv_details);
        mCustomerItemList = new ArrayList<>();
        mCustomerItemList.add(new CustomerItem(1, "My Shop", "017555555", "Mr A"));
        mCustomerItemList.add(new CustomerItem(2, "My Shop", "017555555", "Mr A"));
        mCustomerItemList.add(new CustomerItem(3, "My Shop", "017555555", "Mr A"));

        mCustomerItemList.add(new CustomerItem(1, "My Shop", "017555555", "Mr A"));
        mCustomerItemList.add(new CustomerItem(2, "My Shop", "017555555", "Mr A"));
        mCustomerItemList.add(new CustomerItem(3, "My Shop", "017555555", "Mr A"));
        CustomerListAdapter customerListAdapter = new CustomerListAdapter(getContext(), mCustomerItemList);

        rvCustomerList.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvCustomerList.setLayoutManager(layoutManager);
        rvCustomerList.setAdapter(customerListAdapter);
    }
}
