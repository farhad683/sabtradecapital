package com.farhad.sabtradecapital.customer;

public class CustomerItem {
    private int id;
    private String shopName;
    private String mobile;
    private String addedBy;

    public CustomerItem(int id, String shopName, String mobile, String addedBy) {
        this.id = id;
        this.shopName = shopName;
        this.mobile = mobile;
        this.addedBy = addedBy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }
}
