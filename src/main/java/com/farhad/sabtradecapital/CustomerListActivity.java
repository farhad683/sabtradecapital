package com.farhad.sabtradecapital;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.farhad.sabtradecapital.customer.CustomerItem;
import com.farhad.sabtradecapital.adapter.CustomerListAdapter;
import com.farhad.sabtradecapital.ui.order.OrderListActivity;

import java.util.ArrayList;

public class CustomerListActivity extends AppCompatActivity {

    public String[] areaList = {"Dhaka", "Tangail", "Mirpur"};
    public String[] zoneList = {"zone 1", "zone 2", "zone 3"};
    public String[] filterList = {"filter 1", "filter 2", "filter 3"};

    private Spinner spFilter, spArea, spZone;
    private RecyclerView rvCustomerList;

    ArrayList<CustomerItem> mCustomerItemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_list);
        setTitle("Customer List");

        initializeCustomers();
        initializeFilters();


    }

    private void initializeFilters() {
        spFilter = findViewById(R.id.customer_list_sp_filter);
        spArea = findViewById(R.id.customer_list_sp_area);
        spZone = findViewById(R.id.customer_layout_sp_zone);

        ArrayAdapter filterAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, filterList);
        ArrayAdapter areaAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, areaList);
        ArrayAdapter zoneAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, zoneList);

        spFilter.setAdapter(filterAdapter);
        spArea.setAdapter(filterAdapter);
        spZone.setAdapter(zoneAdapter);
    }


    private void initializeCustomers() {
        rvCustomerList = findViewById(R.id.customer_list_rv_details);
        mCustomerItemList = new ArrayList<>();
        mCustomerItemList.add(new CustomerItem(1, "My Shop", "017555555", "Mr A"));
        mCustomerItemList.add(new CustomerItem(2, "My Shop", "017555555", "Mr A"));
        mCustomerItemList.add(new CustomerItem(3, "My Shop", "017555555", "Mr A"));

        mCustomerItemList.add(new CustomerItem(1, "My Shop", "017555555", "Mr A"));
        mCustomerItemList.add(new CustomerItem(2, "My Shop", "017555555", "Mr A"));
        mCustomerItemList.add(new CustomerItem(3, "My Shop", "017555555", "Mr A"));
        CustomerListAdapter customerListAdapter = new CustomerListAdapter(this, mCustomerItemList);

        rvCustomerList.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvCustomerList.setLayoutManager(layoutManager);
        rvCustomerList.setAdapter(customerListAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.customer_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_order_list:
                startActivity(new Intent(this, OrderListActivity.class));
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
