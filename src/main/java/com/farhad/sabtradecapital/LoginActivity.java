package com.farhad.sabtradecapital;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private EditText edtPhone, edtPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("");

        edtPhone = findViewById(R.id.login_layout_edt_mobile);
        edtPassword = findViewById(R.id.login_layout_edt_password);
        btnLogin = findViewById(R.id.login_layout_btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = edtPhone.getText().toString();
                String pass = edtPassword.getText().toString();
                if(phone.isEmpty() || pass.isEmpty()){
                    Toast.makeText(LoginActivity.this, "Enter phone: 123 and password: 123", Toast.LENGTH_SHORT).show();
                } else {
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    finish();
                }
            }
        });

    }
}
