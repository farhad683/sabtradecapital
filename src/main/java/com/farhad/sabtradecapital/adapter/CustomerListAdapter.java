package com.farhad.sabtradecapital.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital.R;
import com.farhad.sabtradecapital.customer.CustomerItem;
import com.farhad.sabtradecapital.ui.TicketFragment;
import com.farhad.sabtradecapital.ui.customer.CustomerDetailsFragment;

import java.util.ArrayList;

public class CustomerListAdapter extends RecyclerView.Adapter<CustomerListAdapter.CustomerListVH> {
    private ArrayList<CustomerItem> customerList;
    private Context mContext;

    public CustomerListAdapter(Context context, ArrayList<CustomerItem> customerList) {
        this.customerList = customerList;
        mContext = context;
    }

    @NonNull
    @Override
    public CustomerListVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.customer_list_row, parent, false);
        CustomerListVH clvh = new CustomerListVH(v);
        return clvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomerListVH holder, int position) {
        CustomerItem currrentItem = customerList.get(position);
        holder.txtViewID.setText(""+currrentItem.getId());
        holder.txtViewShopName.setText(""+currrentItem.getShopName());
        holder.txtViewMobile.setText(currrentItem.getMobile());
        holder.txtViewAddedBy.setText(currrentItem.getAddedBy());

        holder.btnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(mContext, "Customer details will be shown", Toast.LENGTH_SHORT).show();
                Fragment newFragment = new CustomerDetailsFragment();
                AppCompatActivity activity = (AppCompatActivity) v.getContext();

                FragmentManager fm = activity.getSupportFragmentManager();
                fm.popBackStack();
                FragmentTransaction transaction = fm
                        .beginTransaction();


                transaction.replace(R.id.nav_host_fragment, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });
    }

    @Override
    public int getItemCount() {
        return customerList.size();
    }

    public static class CustomerListVH extends RecyclerView.ViewHolder {
        public TextView txtViewID, txtViewShopName, txtViewMobile, txtViewAddedBy;
        public ImageButton btnDetails;

        public CustomerListVH(@NonNull View itemView) {
            super(itemView);
            txtViewID = itemView.findViewById(R.id.customer_list_id);
            txtViewShopName = itemView.findViewById(R.id.customer_list_shop_name);
            txtViewMobile = itemView.findViewById(R.id.customer_list_mobile);
            txtViewAddedBy = itemView.findViewById(R.id.customer_list_added_by);
            btnDetails = itemView.findViewById(R.id.show_customer_details);

        }
    }
}
