package com.farhad.sabtradecapital.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital.R;
import com.farhad.sabtradecapital.models.ReportModel;

import java.util.ArrayList;
import java.util.List;

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ReportViewHolder> {

    private List<ReportModel> reportModelList;

    public ReportAdapter(List<ReportModel> reportModelList) {
        this.reportModelList = reportModelList;
    }

    @NonNull
    @Override
    public ReportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_layout_item, parent, false);
        return new ReportViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportViewHolder holder, int position) {
        ReportModel currentReport = reportModelList.get(position);
        holder.tvDate.setText(currentReport.getDate());
        holder.tvShopName.setText(currentReport.getShopName());
        holder.tvTotalAmount.setText("" + currentReport.getTotalAmount());
        holder.tvPaidAmount.setText("" + currentReport.getPaidAmount());
        holder.tvDueAmount.setText("" + currentReport.getDueAmount());
    }

    @Override
    public int getItemCount() {
        return reportModelList.size();
    }

    public class ReportViewHolder extends RecyclerView.ViewHolder{

        private TextView tvDate, tvShopName, tvTotalAmount, tvPaidAmount, tvDueAmount;

        public ReportViewHolder(@NonNull View itemView){
            super(itemView);
            tvDate = itemView.findViewById(R.id.report_tv_date);
            tvShopName = itemView.findViewById(R.id.report_tv_shop_name);
            tvTotalAmount = itemView.findViewById(R.id.report_tv_total_amount);
            tvPaidAmount = itemView.findViewById(R.id.report_tv_paid_amount);
            tvDueAmount = itemView.findViewById(R.id.report_tv_due_amount);
        }
    }
}
