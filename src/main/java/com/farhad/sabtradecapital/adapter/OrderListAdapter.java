package com.farhad.sabtradecapital.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital.R;
import com.farhad.sabtradecapital.ui.customer.CustomerDailyDeitalFragment;
import com.farhad.sabtradecapital.ui.customer.CustomerDetailsFragment;
import com.farhad.sabtradecapital.ui.order.OrderModel;

import java.util.ArrayList;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderListVH> {
    private ArrayList<OrderModel> orderList;
    private Context mContext;

    public OrderListAdapter( Context context, ArrayList<OrderModel> orderList) {
        this.orderList = orderList;
        mContext = context;
    }

    @NonNull
    @Override
    public OrderListVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list_row, parent, false);
        OrderListVH orderListVH = new OrderListVH(v);
        return orderListVH;
    }

    @Override
    public void onBindViewHolder(@NonNull OrderListVH holder, int position) {
        OrderModel currentItem = orderList.get(position);
        holder.txtViewID.setText("" + currentItem.getId());
        holder.txtViewDate.setText(currentItem.getDate());
        holder.txtViewShopName.setText(currentItem.getShopName());
        holder.txtSellBy.setText(currentItem.getSellBy());
        holder.txtTotalAmount.setText("" + currentItem.getTotalAmount());
        holder.txtViewAddress.setText("" + currentItem.getAddress());

        if(position % 2 == 1 ){
            holder.tableLayout.setBackgroundColor(Color.LTGRAY);
        }

        holder.order_list_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(mContext, "Invoice will be created.", Toast.LENGTH_SHORT).show();
                Fragment newFragment = new CustomerDailyDeitalFragment();
                AppCompatActivity activity = (AppCompatActivity) v.getContext();

                FragmentManager fm = activity.getSupportFragmentManager();
                fm.popBackStack();
                FragmentTransaction transaction = fm
                        .beginTransaction();


                transaction.replace(R.id.nav_host_fragment, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public static class OrderListVH extends RecyclerView.ViewHolder{
        public TextView txtViewID, txtViewDate, txtViewShopName, txtViewCustName,txtSellBy, txtTotalAmount, txtViewAddress;
        public TextView order_list_details, txtViewPrint;
        public CardView tableLayout;

        public OrderListVH(@NonNull View itemView) {

            super(itemView);

            txtViewID = itemView.findViewById(R.id.order_list_id);
            txtViewDate = itemView.findViewById(R.id.order_list_date);
            txtViewShopName = itemView.findViewById(R.id.order_list_shop_name);
            txtSellBy = itemView.findViewById(R.id.order_list_sell_by);
            txtTotalAmount = itemView.findViewById(R.id.order_list_total_amount);
            txtViewAddress = itemView.findViewById(R.id.order_list_address);

            order_list_details = itemView.findViewById(R.id.order_list_details);
            tableLayout = itemView.findViewById(R.id.table_row);

        }
    }

}
