package com.farhad.sabtradecapital.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.sabtradecapital.R;
import com.farhad.sabtradecapital.customer.CustomerItem;
import com.farhad.sabtradecapital.models.TicketItem;

import java.util.ArrayList;
import java.util.List;

public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.TicketListVH> {
    private List<TicketItem> ticketList;
    private Context mContext;

    public TicketAdapter(Context context, List<TicketItem> ticketList) {
        this.ticketList = ticketList;
        mContext = context;
    }

    @NonNull
    @Override
    public TicketListVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_row, parent, false);
        TicketListVH ticketListVH = new TicketListVH(v);
        return ticketListVH;
    }

    @Override
    public void onBindViewHolder(@NonNull TicketListVH holder, int position) {
        TicketItem currentItem = ticketList.get(position);
        holder.ticket_tv_p_name.setText(currentItem.getProductName());
        holder.ticket_edt_qty.setText(""+ currentItem.getQty());
        holder.ticket_tv_price.setText("" + currentItem.getPrice());
    }

    @Override
    public int getItemCount() {
        return ticketList.size();
    }


    public static class TicketListVH extends RecyclerView.ViewHolder {
        public TextView ticket_tv_p_name, ticket_tv_price;
        public EditText ticket_edt_qty;

        public TicketListVH(@NonNull View itemView) {
            super(itemView);
            ticket_tv_p_name = itemView.findViewById(R.id.ticket_tv_p_name);
            ticket_tv_price = itemView.findViewById(R.id.ticket_tv_price);
            ticket_edt_qty = itemView.findViewById(R.id.ticket_edt_qty);

        }
    }
}