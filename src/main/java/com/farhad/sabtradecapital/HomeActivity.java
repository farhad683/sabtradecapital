package com.farhad.sabtradecapital;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.FrameLayout;

import com.farhad.sabtradecapital.ui.StockFragment;
import com.farhad.sabtradecapital.ui.customer.CustomerFragment;
import com.farhad.sabtradecapital.ui.home.HomeFragment;
import com.farhad.sabtradecapital.ui.order.OrderListFragment;
import com.farhad.sabtradecapital.ui.report.AllReportFragment;
import com.farhad.sabtradecapital.ui.sales_order.SalesOrderFragment;
import com.farhad.sabtradecapital.ui.stock.StockActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ReportFragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final int HOME_FRAGMENT = 0;
    private static final int CUSTOMER_FRAGMENT = 1;
    private static final int ORDER_LIST_FRAGMENT = 2;
    private static final int SALES_ORDER_FRAGMENT = 3;
    private static final int STOCK_FRAGMENT = 4;
    private static final int REPORT_FRAGMENT = 5;

    private int currentFragment = -1;
    

    private AppBarConfiguration mAppBarConfiguration;
    private BottomNavigationView bottomNav;
    
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);
        

        bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        
        frameLayout = findViewById(R.id.nav_host_fragment);

        //I added this if statement to keep the selected fragment when rotating the device
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                    new HomeFragment()).commit();
        }

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        setFragment(new HomeFragment(), HOME_FRAGMENT);
    }

    private void setFragment(Fragment fragment, int fragmentNo){
        if(fragmentNo != currentFragment){
            currentFragment = fragmentNo;
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(frameLayout.getId(), fragment);
            fragmentTransaction.commit();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;
                    switch (item.getItemId()) {
                        case R.id.nav_home:
                            selectedFragment = new HomeFragment();
                            break;
                        case R.id.nav_order_list:
                            selectedFragment = new OrderListFragment();
                            break;
                        case R.id.nav_create_order:
                            selectedFragment = new SalesOrderFragment();
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
                            selectedFragment).commit();
                    return true;
                }
            };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);

        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.nav_home:
                invalidateOptionsMenu();
                setFragment(new HomeFragment(), HOME_FRAGMENT);
                break;
            case R.id.nav_customer:
                invalidateOptionsMenu();
                gotoFragment("Customers", new CustomerFragment(), CUSTOMER_FRAGMENT);
                break;
            case R.id.nav_order:
                invalidateOptionsMenu();
                gotoFragment("Order List", new OrderListFragment(), ORDER_LIST_FRAGMENT);
                break;
            case R.id.nav_sales_order:
                invalidateOptionsMenu();
                gotoFragment("Create Order", new SalesOrderFragment(), SALES_ORDER_FRAGMENT);
                break;
            case R.id.nav_stock:
                invalidateOptionsMenu();
//                gotoFragment("Stock", new StockFragment(), STOCK_FRAGMENT);
                startActivity(new Intent(this, StockActivity.class));
                break;
            case R.id.nav_report:
                invalidateOptionsMenu();
                gotoFragment("Report", new AllReportFragment(), REPORT_FRAGMENT);
                break;

        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void gotoFragment(String title, Fragment fragment, int fragmentNo) {
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(title);
        invalidateOptionsMenu();
        setFragment(fragment, fragmentNo);
    }
}
